### Available on GooglePlay
[Google Play](https://play.google.com/store/apps/details?id=de.fh_stralsund.myodrone)

## MyoDrone
##### von Alexander Zaak, Max Edenharter und Tobias Ludwig

### 1. Einleitung
Ziel des Projektes war es die Drohne AR.Drone 2.0 per Gesten zu steuern. Für die Analyse der Gesten und Bewegungen kam das Armband Myo zum Einsatz. Dies ermöglicht elektrische Signale von Muskeln durch Bewegungssensoren und Elektroden zu erkennen. Als Kommunikationsschnittstelle zwischen Myo-Armband und AR.Drone wurde ein Smartphone mit einem Android-Betriebssystem verwendet. Myo-Armband und AR.Drone haben keinen direkten Kontakt zueinander. Um die Bewegungsmuster des Myo-Armbands für die AR.Drone verständlich zu machen, wurde ein eigenständigen Applikation „MyoDrone" realisiert.

### 2. Funktionsumfang
* Steuerung der AR.Drone durch Gesten / Bewegungen
* Start / Landung —> doppeltes Fingertippen
* Steigen / Sinken —> gespreizte Hand + vertikale Armbewegung
* Vorwärts / Rückwärts —> Faust + abkippende / anziehende Armbewegung
* Rechts / Links —> gespreizte Hand + drehende Armbewegung
* Rotation links / rechts —> Welle links / rechts
* Steuerung der AR.Drone durch das Smartphone
* Abgesichertes Starten —> Start-Button
* Landen —> Lande-Button
* Notaus —> Notaus-Button
* Analyse und Anzeige von Statusdaten des Myo-Armbandes
* Verbindungsstatus
* Verbindungsqualität
* Anzeige der aktuellen Pose
* Analyse und Anzeige von Statusdaten der AR.Drone
* Batteriekapazität
* Verbindungsstatus
* Verbindungsqualität
* Einstellung von Bewegungsgeschwindigkeit der AR.Drone
* Tutorial für die zu verwendenden Posen
* Anzeige der erwartende und erkannten Pose
* Zähleranzeige für erkannten und gescheiterten Posen