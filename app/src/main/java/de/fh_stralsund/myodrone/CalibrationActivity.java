package de.fh_stralsund.myodrone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thalmic.myo.Pose;

import java.io.Serializable;

import de.fh_stralsund.myodrone.fragment.CalibrationFragment;
import de.fh_stralsund.myodrone.layout.NonSwipeableViewPager;
import de.fh_stralsund.myodrone.utils.Const;


public class CalibrationActivity extends FragmentActivity implements CalibrationFragment.OnFragmentInteractionListener {

    private Pose[] posesToCheck = new Pose[]{Pose.FIST, Pose.FINGERS_SPREAD, Pose.WAVE_IN, Pose.WAVE_OUT};

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private NonSwipeableViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private ScreenSlidePagerAdapter mPagerAdapter;

    /**
     * Basic Activity initialization
     *
     * @param savedInstanceState state of the activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);
        // Instantiate a ViewPager and a PagerAdapter.
        initView();
    }

    private void initView() {
        mPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }


    /**
     * Method called, when next-button clicked
     *
     * @param isRecognized true, when pose are recognized, else false
     */
    @Override
    public void onNextButtonClicked(boolean isRecognized) {
        if (isRecognized) {
            if (mPagerAdapter.getCount() == mPager.getCurrentItem() + 1) {
                finishCalibration(false);
                return;
            }
            mPager.setCurrentItem(mPager.getCurrentItem() + 1);
        } else {
            finishCalibration(true);
        }
    }

    /**
     * Method called when activity are destroyed
     */
    @Override
    public void onDestroy() {
        if (mPagerAdapter != null && mPagerAdapter.getCurrentFragment() != null)
            mPagerAdapter.getCurrentFragment().resetFailCount();
        super.onDestroy();
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        initView();
        super.onResume();
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        finishCalibration(true);
    }

    /**
     * Method to finish calibration
     *
     * @param isFailed true, when calibration are failed, else false
     */
    private void finishCalibration(boolean isFailed) {
        Intent intent = new Intent(this, DeviceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Const.IS_CALIBRATION_FAILED, isFailed);
        startActivity(intent);
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter implements Serializable {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private CalibrationFragment currentFragment;

        @Override
        public Fragment getItem(int position) {
            currentFragment = new CalibrationFragment().newInstance(posesToCheck[position]);
            return currentFragment;
        }

        @Override
        public int getCount() {
            return posesToCheck.length;
        }

        public CalibrationFragment getCurrentFragment() {
            return currentFragment;
        }
    }
}
