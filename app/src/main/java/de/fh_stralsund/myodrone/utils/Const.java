package de.fh_stralsund.myodrone.utils;

import com.thalmic.myo.Pose;

import java.util.HashMap;
import java.util.Map;

import de.fh_stralsund.myodrone.R;

/**
 * Created by Alexander Zaak on 12.05.15.
 * <p/>
 * Class for constants
 */
public class Const {

    public static final String IS_PRESSED = "is_pressed";
    public static final String LOG_DIRECTORY = "MyoDrone/Log";
    public static final String SERVICE_STATUS = "service_status";
    public static final String IS_MYO_CONNECTED = "is_myo_connected";
    public static final String IS_MYO_LOCKED = "is_myo_locked";
    public static final String MYO_POSE = "myo_pose";
    public static final String MYO_RSSI = "myo_rssi";
    public static final String EXPECTED_POSE = "expected_pose";
    public static final int SETTINGS_RESULT = 200;
    public static final int DRONE_SPEED = 20;

    public static final Map<Integer, Integer> POSES = new HashMap<Integer, Integer>();

    static {
        POSES.put(Pose.REST.ordinal(), R.mipmap.icon_white_outline_rh_rest);
        POSES.put(Pose.FIST.ordinal(), R.mipmap.icon_white_outline_rh_fist);
        POSES.put(Pose.WAVE_IN.ordinal(), R.mipmap.icon_white_outline_rh_wave_left);
        POSES.put(Pose.WAVE_OUT.ordinal(), R.mipmap.icon_white_outline_rh_wave_right);
        POSES.put(Pose.FINGERS_SPREAD.ordinal(), R.mipmap.icon_white_outline_rh_spread_fingers);
        POSES.put(Pose.DOUBLE_TAP.ordinal(), R.mipmap.icon_white_outline_rh_double_tap);
        POSES.put(Pose.UNKNOWN.ordinal(), R.mipmap.icon_white_outline_unknown);
    }

    public static final String IS_CALIBRATION_FAILED = "is_calibration_failed";
    public static final String IS_INITIALIZED = "is_initialized";
    public static final String IS_MYO_SYNCED = "is_myo_sunced";
    public static final String MYO_SYNC_STATUS = "myo_sync_status";

    public static final long[] PATTERN = {0, 100, 1000, 300, 200, 100, 500, 200, 100};
    public static final String IS_PAUSED = "is_paused";
    public static final int DEFAULT_SPEED = 80;
    public static final String CURRENT_CALIBRATION_POSITION = "current_calibration_position";
    public static final String FRAGMENT_CALIBRATION = "fragment_calibration";
    public static final String FRAGMENT_DEVICE_MANAGER = "fragment_device_manager";
    public static final String FRAGMENT_MAIN = "fragment_main";
    public static final String SCREEN_SLIDE_PAGER_ADAPTER = "screen_slide_pager_adapter";
}
