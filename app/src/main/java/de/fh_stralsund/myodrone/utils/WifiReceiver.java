package de.fh_stralsund.myodrone.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Alexander Zaak on 20.05.15.
 */
public class WifiReceiver extends BroadcastReceiver {


    private OnDroneConnectedListener mListener;

    public void setListener(OnDroneConnectedListener mFragment) {
        this.mListener = mFragment;
    }

   /* public void setListener(DeviceFragment mFragment) {
        this.mListener = mFragment;
    }*/

    public interface OnDroneConnectedListener {
        public void OnWifiInfoChanged(boolean isDroneNetwork, int signalStrength);
    }


    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.
     *
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (info != null) {
            try {
                if (info.isConnected()) {

                    // e.g. To check the Network Name or other info:
                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 10);
                    int percentage = (int) ((level / 10.0) * 100);

                    int ip = wifiInfo.getIpAddress();

                    boolean checkSSID = wifiInfo.getSSID().contains("ardrone");
                    boolean checkIP = parseIpAddress(ip);

                    if (mListener != null) {
                        mListener.OnWifiInfoChanged(checkSSID && checkIP, percentage);
                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to parse an ip from wifiInfo
     * and check the current Ip
     * @see android.net.wifi.WifiInfo
     * @param ip from wifiInfo
     * @return true,if connection correct, else false
     * @throws UnknownHostException
     */
    private boolean parseIpAddress(int ip) throws UnknownHostException {
        if (ip == 0) {
            return false;
        }
        int reversedIp = Integer.reverseBytes(ip);
        byte[] ipAddress = BigInteger.valueOf(reversedIp).toByteArray();

        InetAddress address = InetAddress.getByAddress(ipAddress);

        return address.getHostAddress().equals("192.168.1.2");
    }

}
