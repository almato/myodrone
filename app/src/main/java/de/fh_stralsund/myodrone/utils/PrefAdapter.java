package de.fh_stralsund.myodrone.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import de.fh_stralsund.myodrone.R;

/**
 * Created by Alexander Zaak on 03.06.15.
 *
 * Class to managing preferences
 */
public class PrefAdapter {

    private SharedPreferences prefs;
    private Context context;

    public PrefAdapter(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        loadDefaultPrefs();
    }

    public boolean isTutorialEnabled() {
        return prefs.getBoolean(context.getString(R.string.pref_key_tutorial), false);
    }

    public boolean isCalibrationFailed() {
        return prefs.getBoolean(context.getString(R.string.pref_key_calibration_failed), false);
    }

    public void setIsCalibrationFailed(boolean isCalibrationFailed) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(context.getString(R.string.pref_key_calibration_failed), isCalibrationFailed);
        editor.apply();
    }

    public int getDroneSpeed() {
        return prefs.getInt(context.getString(R.string.pref_key_drone_speed), Const.DRONE_SPEED);
    }

    public void setDroneSpeed(int speed) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(context.getString(R.string.pref_key_drone_speed), speed);
        editor.apply();
    }

    /**
     * Method for loading default Preferences
     */
    private void loadDefaultPrefs() {
        // Load preferences
        PreferenceManager.setDefaultValues(this.context, R.xml.preferences, false);
    }


}
