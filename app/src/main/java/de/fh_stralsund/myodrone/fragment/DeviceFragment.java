package de.fh_stralsund.myodrone.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thalmic.myo.scanner.ScanActivity;

import de.fh_stralsund.myodrone.R;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.MyoReceiver;
import de.fh_stralsund.myodrone.utils.WifiReceiver;


/**
 * Created by Alexander Zaak on 12.05.15.
 * <p/>
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DeviceFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DeviceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeviceFragment extends Fragment implements WifiReceiver.OnDroneConnectedListener, MyoReceiver.OnMyoInteractionListener {

    private OnFragmentInteractionListener mListener;
    private WifiReceiver wifiReceiver;
    private MyoReceiver myoReceiver;

    //GUI - Elements
    private TextView statusDrone;
    private TextView statusMyo;
    private Button btnWifiSettings;
    private Button btnMyoScan;
    private Button btnNext;

    private RelativeLayout myoStatusOn;
    private RelativeLayout myoStatusOff;

    private RelativeLayout droneStatusOn;
    private RelativeLayout droneStatusOff;

    private ProgressBar progressDroneInit;

    private boolean isDroneConnected = false;
    private boolean isMyoConnected = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DeviceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeviceFragment newInstance() {
        DeviceFragment fragment = new DeviceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public DeviceFragment() {
        // Required empty public constructor
    }

    /**
     * Basic fragment initialization
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Method to create view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_device, container, false);

        myoStatusOn = (RelativeLayout) view.findViewById(R.id.container_myo_connection_status_on);
        myoStatusOff = (RelativeLayout) view.findViewById(R.id.container_myo_connection_status_off);
        droneStatusOn = (RelativeLayout) view.findViewById(R.id.container_drone_connection_status_on);
        droneStatusOff = (RelativeLayout) view.findViewById(R.id.container_drone_connection_status_off);

        statusDrone = (TextView) view.findViewById(R.id.status_drone);
        statusMyo = (TextView) view.findViewById(R.id.status_myo);
        btnWifiSettings = (Button) view.findViewById(R.id.btn_wifi_settings);
        btnMyoScan = (Button) view.findViewById(R.id.btn_myo_scan);
        btnNext = (Button) view.findViewById(R.id.btn_drone_init);

        progressDroneInit = (ProgressBar) view.findViewById(R.id.progressbar_drone_init);

        btnWifiSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        btnMyoScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScanActivity.class));
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDroneInit.setVisibility(View.VISIBLE);
                mListener.onNextButtonClicked();
            }
        });

        return view;
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param activity current activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link android.app.Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(wifiReceiver);
        getActivity().unregisterReceiver(myoReceiver);

    }


    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link android.app.Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {

        if (progressDroneInit.getVisibility() == View.VISIBLE) {
            progressDroneInit.setVisibility(View.GONE);
        }

        wifiReceiver = new WifiReceiver();
        myoReceiver = new MyoReceiver();

        wifiReceiver.setListener(this);
        myoReceiver.setListener(this);

        getActivity().registerReceiver(wifiReceiver, new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION));
        getActivity().registerReceiver(wifiReceiver, new IntentFilter(
                WifiManager.RSSI_CHANGED_ACTION));

        getActivity().registerReceiver(myoReceiver, new IntentFilter(Const.SERVICE_STATUS));

        super.onResume();
    }

    /**
     * Called when wifi state are changed
     *
     * @param isDroneNetwork
     * @param signalStrength
     */
    @Override
    public void OnWifiInfoChanged(boolean isDroneNetwork, int signalStrength) {
        isDroneConnected = isDroneNetwork;
        if (getView() != null && isDroneNetwork) {
            btnWifiSettings.setEnabled(false);

        } else if (getView() != null) {
            btnWifiSettings.setEnabled(true);
        }
        updateDroneView(isDroneNetwork);
        updateNextBtn();
    }

    /**
     * Called  when new are recognized
     *
     * @param isConnected
     * @param pose
     */
    @Override
    public void onPoseChanged(boolean isConnected, int pose) {
        isMyoConnected = isConnected;
        if (getView() != null && isConnected) {
            btnMyoScan.setEnabled(false);
        } else if (getView() != null) {
            btnMyoScan.setEnabled(true);
        }
        updateMyoView(isConnected);
        updateNextBtn();
    }


    public void updateNextBtn() {
        if (getView() != null && isMyoConnected && isDroneConnected) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
    }

    @Override
    public void onMyoSynced(boolean isSynced) {
//nothing to do
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onNextButtonClicked();
    }

    private void updateMyoView(boolean status) {
        if (status) {
            myoStatusOn.setVisibility(View.VISIBLE);
            myoStatusOff.setVisibility(View.GONE);
        } else {
            myoStatusOn.setVisibility(View.GONE);
            myoStatusOff.setVisibility(View.VISIBLE);
        }
    }

    private void updateDroneView(boolean status) {
        if (status) {
            droneStatusOn.setVisibility(View.VISIBLE);
            droneStatusOff.setVisibility(View.GONE);
        } else {
            droneStatusOn.setVisibility(View.GONE);
            droneStatusOff.setVisibility(View.VISIBLE);
        }
    }

}
