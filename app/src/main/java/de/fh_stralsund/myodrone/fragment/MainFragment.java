package de.fh_stralsund.myodrone.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import de.fh_stralsund.myodrone.R;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.Messages;
import de.fh_stralsund.myodrone.utils.MyoReceiver;
import de.fh_stralsund.myodrone.utils.PrefAdapter;
import de.fh_stralsund.myodrone.utils.WifiReceiver;
import de.fh_stralsund.myodrone.yaDroneUtil.DroneMove;
import de.fh_stralsund.myodrone.yaDroneUtil.YaDroneTask;
import de.yadrone.base.IARDrone;
import de.yadrone.base.navdata.BatteryListener;


/**
 * Created by Alexander Zaak on 12.05.15.
 * <p/>
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements WifiReceiver.OnDroneConnectedListener, MyoReceiver.OnMyoInteractionListener {

    private static final String TAG = MainFragment.class.getName();
    private OnFragmentInteractionListener mListener;
    private WifiReceiver wifiReceiver;
    private MyoReceiver myoReceiver;
    private static IARDrone drone;

    private ProgressBar progressBarDroneBatteryCapacity;
    private TextView progressBarDroneBatteryCapacityStatusText;

    private ProgressBar progressBarConnectionQuality;
    private TextView progressBarConnectionQualityStatusText;

    private RelativeLayout containerDroneConnectionStatusOn;
    private RelativeLayout containerDroneConnectionStatusOff;

    private RelativeLayout containerMyoConnectionStatusOn;
    private RelativeLayout containerMyoConnectionStatusOff;

    private SeekBar speedSeekBar;
    private TextView speedSeekBarStatusText;

    private ImageView imgPose;

    private boolean isDroneOnline = false;

    private BatteryListener batteryListener;

    private PrefAdapter prefAdapter;

    private Vibrator vibrator;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * .
     *
     * @return A new instance of fragment MainFragment.
     */
    public static MainFragment newInstance(IARDrone drone) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        MainFragment.drone = drone;
        return fragment;
    }

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Basic fragment initialization
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefAdapter = new PrefAdapter(this.getActivity());
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

    }

    /**
     * Called when wifi-state are changed
     * @param isDroneNetwork
     * @param signalStrength
     */
    @Override
    public void OnWifiInfoChanged(final boolean isDroneNetwork, final int signalStrength) {
        isDroneOnline = isDroneNetwork;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int mSignalStrength = 0;
                if (isDroneNetwork && signalStrength > 0) {
                    mSignalStrength = signalStrength;
                    containerDroneConnectionStatusOff.setVisibility(View.GONE);
                    containerDroneConnectionStatusOn.setVisibility(View.VISIBLE);
                } else {

                    vibrator.vibrate(Const.PATTERN, 0);

                    Messages.droneNotConnectedMessage(getActivity(), vibrator);

                    containerDroneConnectionStatusOn.setVisibility(View.GONE);
                    containerDroneConnectionStatusOff.setVisibility(View.VISIBLE);
                }
                progressBarConnectionQuality.setProgress(mSignalStrength);
                progressBarConnectionQualityStatusText.setText(mSignalStrength + getString(R.string.percent));
            }
        });
    }

    /**
     * called when new pose are recognized
     * @param isConnected
     * @param pose
     */
    @Override
    public void onPoseChanged(final boolean isConnected, final int pose) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isConnected) {
                    containerMyoConnectionStatusOff.setVisibility(View.GONE);
                    containerMyoConnectionStatusOn.setVisibility(View.VISIBLE);
                } else {
                    Messages.myoNotConnectedMessage(getActivity());
                    containerMyoConnectionStatusOn.setVisibility(View.GONE);
                    containerMyoConnectionStatusOff.setVisibility(View.VISIBLE);
                }

                imgPose.setImageResource(Const.POSES.get(pose));
            }
        });
    }

    /**
     * Called when sync-state of myo-armband are changed
     * @param isSynced
     */
    @Override
    public void onMyoSynced(final boolean isSynced) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isSynced) {
                    containerMyoConnectionStatusOff.setVisibility(View.GONE);
                    containerMyoConnectionStatusOn.setVisibility(View.VISIBLE);
                } else {
                    containerMyoConnectionStatusOn.setVisibility(View.GONE);
                    containerMyoConnectionStatusOff.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * Listener for Activity interactions
     */
    public interface OnFragmentInteractionListener {
        public void onButtonPressed(boolean isPressed);
    }

    /**
     * Method to create view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Button landingButton = (Button) view.findViewById(R.id.btn_landing);
        landingButton.setOnClickListener(createLandingOnClickListener());

        Button initButton = (Button) view.findViewById(R.id.btn_init);
        initButton.setOnTouchListener(createInitOnTouchListener());

        Button killButton = (Button) view.findViewById(R.id.btn_killswitch);
        killButton.setOnLongClickListener(createKillOnClickListener());

        progressBarDroneBatteryCapacity = (ProgressBar) view.findViewById(R.id.progressbar_drone_battery_capacity);
        progressBarDroneBatteryCapacity.setProgress(0);
        progressBarDroneBatteryCapacity.setMax(100);
        progressBarDroneBatteryCapacityStatusText = (TextView) view.findViewById(R.id.progressbar_drone_battery_capacity_statustext);

        containerDroneConnectionStatusOn = (RelativeLayout) view.findViewById(R.id.container_drone_connection_status_on);
        containerDroneConnectionStatusOff = (RelativeLayout) view.findViewById(R.id.container_drone_connection_status_off);

        progressBarConnectionQuality = (ProgressBar) view.findViewById(R.id.progressbar_drone_connection_quality);
        progressBarConnectionQuality.setProgress(0);
        progressBarConnectionQuality.setMax(100);
        progressBarConnectionQualityStatusText = (TextView) view.findViewById(R.id.progressbar_drone_connection_quality_statustext);

        containerMyoConnectionStatusOn = (RelativeLayout) view.findViewById(R.id.container_myo_connection_status_on);
        containerMyoConnectionStatusOff = (RelativeLayout) view.findViewById(R.id.container_myo_connection_status_off);

        speedSeekBar = (SeekBar) view.findViewById(R.id.seekbar_drone_speed);
        speedSeekBar.setMax(100);
        speedSeekBar.setProgress(prefAdapter.getDroneSpeed());
        speedSeekBar.setOnSeekBarChangeListener(onSpeedSeekBarChangeListener());

        speedSeekBarStatusText = (TextView) view.findViewById(R.id.seekbar_drone_speed_statustext);
        speedSeekBarStatusText.setText(String.valueOf(prefAdapter.getDroneSpeed()) + "%");

        imgPose = (ImageView) view.findViewById(R.id.imgPose);


        return view;
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param activity current activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link android.app.Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(wifiReceiver);
        getActivity().unregisterReceiver(myoReceiver);
        drone.getNavDataManager().removeBatteryListener(batteryListener);
    }


    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link android.app.Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        wifiReceiver = new WifiReceiver();
        myoReceiver = new MyoReceiver();

        wifiReceiver.setListener(this);
        myoReceiver.setListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);

        getActivity().registerReceiver(wifiReceiver, intentFilter);

        getActivity().registerReceiver(myoReceiver, new IntentFilter(Const.SERVICE_STATUS));
        if (drone != null) {
            batteryListener = createBatteryListener();
            drone.getNavDataManager().addBatteryListener(batteryListener);
        }
        super.onResume();
    }

    /**
     * Listener for drone battery
     * @return
     */
    private BatteryListener createBatteryListener() {
        return new BatteryListener() {
            @Override
            public void batteryLevelChanged(final int capacity) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        int mCapacity = 0;
                        if (isDroneOnline) {
                            mCapacity = capacity;
                        }
                        progressBarDroneBatteryCapacity.setProgress(mCapacity);
                        progressBarDroneBatteryCapacityStatusText.setText(Integer.toString(mCapacity) + getString(R.string.percent));
                    }
                });
            }

            @Override
            public void voltageChanged(int i) {
                //TODO: Implement UI data refresh
            }
        };
    }

    private View.OnClickListener createLandingOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(50);
                YaDroneTask yaDroneTask = new YaDroneTask(drone, 0);
                yaDroneTask.execute(DroneMove.LANDING);
            }
        };
    }

    private View.OnTouchListener createInitOnTouchListener() {
        return new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mListener.onButtonPressed(true);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    mListener.onButtonPressed(false);
                    vibrator.vibrate(50);
                }
                return false;
            }
        };
    }

    private View.OnLongClickListener createKillOnClickListener() {
        return new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                vibrator.vibrate(50);
                YaDroneTask yaDroneTask = new YaDroneTask(drone, 0);
                yaDroneTask.execute(DroneMove.EMERGENCY);
                return true;
            }
        };
    }

    private SeekBar.OnSeekBarChangeListener onSpeedSeekBarChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                speedSeekBarStatusText.setText(String.valueOf(progress) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                prefAdapter.setDroneSpeed(seekBar.getProgress());
            }
        };
    }


}
