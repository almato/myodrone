package de.fh_stralsund.myodrone.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thalmic.myo.Pose;

import de.fh_stralsund.myodrone.R;
import de.fh_stralsund.myodrone.services.MyoService;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.Messages;
import de.fh_stralsund.myodrone.utils.MyoReceiver;


/**
 * Created by Max Edenharter on 15.05.15.
 * <p/>
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalibrationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalibrationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalibrationFragment extends Fragment implements MyoReceiver.OnMyoInteractionListener {
    private int expectedPose;
    private int recognizedCount;
    private static int failCount = 0;
    private boolean isRecognized = false;

    private MyoReceiver myoReceiver;

    private ProgressBar failedProgress;
    private ProgressBar recognizedProgress;
    private ImageView expectedPoseImageView;
    private ImageView recognizedPoseImageView;
    private TextView failedPercentage;
    private TextView recognizedPercentage;
    private Button nextButton;
    private TextView calibrationStatusText;

    private OnFragmentInteractionListener mListener;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param expPose Parameter 1.
     * @return A new instance of fragment TutorialFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalibrationFragment newInstance(Pose expPose) {
        CalibrationFragment fragment = new CalibrationFragment();
        Bundle args = new Bundle();
        args.putInt(Const.EXPECTED_POSE, expPose.ordinal());
        fragment.setArguments(args);

        return fragment;
    }

    public CalibrationFragment() {
        // Required empty public constructor
    }

    /**
     * Basic fragment initialization
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            expectedPose = getArguments().getInt(Const.EXPECTED_POSE, Pose.UNKNOWN.ordinal());
            recognizedCount = 0;
        }
        Intent intent = new Intent(getActivity(), MyoService.class);
        intent.putExtra(Const.IS_INITIALIZED, true);
        getActivity().startService(intent);
    }

    /**
     * Method to create view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_tutorial, container, false);
        expectedPoseImageView = (ImageView) rootView.findViewById(R.id.img_expected_pose);
        recognizedPoseImageView = (ImageView) rootView.findViewById(R.id.img_current_pose);
        nextButton = (Button) rootView.findViewById(R.id.btn_next_step);
        recognizedPercentage = (TextView) rootView.findViewById(R.id.progressbar_recognized_poses_statustext);
        failedPercentage = (TextView) rootView.findViewById(R.id.progressbar_failed_poses_statustext);

        failedProgress = (ProgressBar) rootView.findViewById(R.id.progressbar_failed_poses);
        failedProgress.setMax(300);
        failedProgress.setProgress(0);
        recognizedProgress = (ProgressBar) rootView.findViewById(R.id.progressbar_recognized_poses);
        recognizedProgress.setMax(300);
        recognizedProgress.setProgress(0);

        expectedPoseImageView.setImageResource(Const.POSES.get(expectedPose));

        calibrationStatusText = (TextView) rootView.findViewById(R.id.calibration_status_text);
        Spanned spanned = Html.fromHtml(getString(R.string.myo_not_synced));
        calibrationStatusText.setMovementMethod(LinkMovementMethod.getInstance());
        calibrationStatusText.setText(spanned);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNextButtonClicked(isRecognized);
            }
        });

        return rootView;
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(android.os.Bundle)} will be called after this.
     *
     * @param activity current activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Called when the fragment is no longer attached to its activity.  This
     * is called after {@link #onDestroy()}.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link android.app.Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        myoReceiver = new MyoReceiver();
        myoReceiver.setListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.SERVICE_STATUS);
        intentFilter.addAction(Const.MYO_SYNC_STATUS);

        getActivity().registerReceiver(myoReceiver, new IntentFilter(Const.SERVICE_STATUS));
        super.onResume();
    }

    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link android.app.Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(myoReceiver);
        super.onPause();
    }


    /**
     * Method called when pose are changed
     *
     * @param isConnected true, if myo-armband is connected
     * @param pose        current pose
     */
    @Override
    public void onPoseChanged(boolean isConnected, int pose) {

        if (!isConnected) {
            Messages.myoNotConnectedMessage(getActivity());
            return;
        }

        if (!getUserVisibleHint() || failCount == 300 || recognizedCount == 300) {
            return;
        }

        recognizedPoseImageView.setImageResource(Const.POSES.get(pose));
        if (pose == expectedPose) {
            recognizedCount++;
        } else {
            recognizedCount = 0;
            if (pose != Pose.REST.ordinal()) {
                failCount++;
            }
        }
        if (recognizedCount == 300) {
            isRecognized = true;
            calibrationStatusText.setText(R.string.calibration_step_success);
            nextButton.setEnabled(true);
        }

        if (failCount == 300) {
            isRecognized = false;
            Spanned spanned = Html.fromHtml(getString(R.string.calibration_failed));
            calibrationStatusText.setMovementMethod(LinkMovementMethod.getInstance());
            calibrationStatusText.setText(spanned);
            nextButton.setEnabled(true);
        }

        recognizedProgress.setProgress(recognizedCount);
        failedProgress.setProgress(failCount);

        recognizedPercentage.setText(String.valueOf(recognizedCount));
        failedPercentage.setText(String.valueOf(failCount));
    }

    /**
     * Method to reset failCount
     */
    public void resetFailCount() {
        failCount = 0;
    }

    /**
     * Method called on Myo-Armband synchronisation
     *
     * @param isSynced true, when Myo-Armband synced, else false
     */
    @Override
    public void onMyoSynced(boolean isSynced) {
        Spanned spanned;
        if (isSynced) {
            spanned = Html.fromHtml(getString(R.string.calibration_intro));

        } else {
            spanned = Html.fromHtml(getString(R.string.myo_not_synced));
        }

        calibrationStatusText.setMovementMethod(LinkMovementMethod.getInstance());
        calibrationStatusText.setText(spanned);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onNextButtonClicked(boolean isPoseRecognized);
    }


}
