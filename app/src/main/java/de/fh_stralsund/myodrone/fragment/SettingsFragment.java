package de.fh_stralsund.myodrone.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import de.fh_stralsund.myodrone.R;

/**
 * Created by Alexander Zaak on 12.05.15.
 */
public class SettingsFragment extends PreferenceFragment {


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
