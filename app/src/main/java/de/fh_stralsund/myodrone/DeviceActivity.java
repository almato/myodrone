package de.fh_stralsund.myodrone;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import de.fh_stralsund.myodrone.controller.DroneController;
import de.fh_stralsund.myodrone.fragment.DeviceFragment;
import de.fh_stralsund.myodrone.services.MyoService;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.PrefAdapter;


public class DeviceActivity extends Activity implements DeviceFragment.OnFragmentInteractionListener {

    private PrefAdapter pref;
    private FragmentManager fragmentManager;
    private DeviceFragment fragment;

    /**
     * Basic Activity initialization
     *
     * @param savedInstanceState state of the activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = new PrefAdapter(this);

        fragmentManager = getFragmentManager();


        instantiateFragments(savedInstanceState);

        this.startService(new Intent(this, MyoService.class));

        if (pref.isTutorialEnabled()) {
            Intent intent = new Intent(this, CalibrationActivity.class);
            startActivity(intent);
        }
    }


    /**
     * Method called, when activity was recreated
     *
     * @param inState saved state of the fragment
     */
    @Override
    protected void onRestoreInstanceState(Bundle inState) {
        instantiateFragments(inState);
    }

    /**
     * Method called, when activity goes sleep saves the current instance of the fragment
     *
     * @param outState current instance
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the fragment's instance
        fragmentManager.putFragment(outState, Const.FRAGMENT_DEVICE_MANAGER, fragment);
    }


    /**
     * Method to create Options-Menu
     *
     * @param menu current menu
     * @return true to allow create menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device, menu);
        return true;
    }

    /**
     * Method called, when options-Item Selected
     *
     * @param item selected menu-item
     * @return return true, if menu-item selected, else false
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_calibration) {
            startActivity(new Intent(this, CalibrationActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method called when next-button are clicked
     */
    @Override
    public void onNextButtonClicked() {
        initDrone();
    }

    /**
     * Method called when an intent are send
     *
     * @param intent current intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra(Const.IS_CALIBRATION_FAILED, true)) {
            this.stopService(new Intent(this, MyoService.class));
            finish();
        }

    }

    /**
     * Method to initiate the Fragment of the Activity
     *
     * @param inState current state of the fragment
     */
    private void instantiateFragments(Bundle inState) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (inState != null) {
            fragment = (DeviceFragment) fragmentManager.getFragment(inState, Const.FRAGMENT_DEVICE_MANAGER);
        } else {
            fragment = new DeviceFragment();
            fragmentTransaction.add(R.id.container, fragment, Const.FRAGMENT_DEVICE_MANAGER);
            fragmentTransaction.commit();
        }
    }

    /**
     * Method to init drone
     */
    private void initDrone() {
        new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object... params) {
                DroneController.initDrone();
                return null;
            }

            @Override
            protected void onPreExecute() {
                findViewById(R.id.progressbar_drone_init).setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void param) {
                findViewById(R.id.progressbar_drone_init).setVisibility(View.GONE);
                startActivity(new Intent(DeviceActivity.this, MainActivity.class));
            }
        }.execute();
    }
}
