package de.fh_stralsund.myodrone.controller;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;

/**
 * Created by Alexander Zaak on 28.05.15.
 */
public class DroneController {

    private static IARDrone mDrone;

    /**
     * Method to init drone
     */
    public static void initDrone() {
        mDrone = new ARDrone("192.168.1.1", null); // null because of missing video support on Android

        try {
            mDrone.start();
        } catch (Exception e) {
            if (mDrone != null) {
                mDrone.stop();
                mDrone = null;
            }
        }
    }

    public static synchronized IARDrone getARDrone() {
        return mDrone;
    }

    /**
     * Method to stop drone
     */
    public static void destroyDrone() {
        mDrone.stop();
    }
}
