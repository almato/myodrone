package de.fh_stralsund.myodrone.controller;

import android.app.Application;
import android.content.Context;
import android.os.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import ch.qos.logback.classic.android.BasicLogcatConfigurator;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.LoggerConfig;

/**
 * Created by Alexander Zaak on 07.04.15.
 */
public class App extends Application {

    static {
        BasicLogcatConfigurator.configureDefaultContext();
    }

    Logger logger = LoggerFactory.getLogger(App.class);
    /**
     * The drone is kept in the application context so that all activities use the same drone instance
     */

    private static App mInstance;
    private static Context context;

    public void onCreate() {
        mInstance = this;
        new LoggerConfig().configure();

        //create
        File log = Environment.getExternalStoragePublicDirectory(Const.LOG_DIRECTORY);
        createWorkingDir(log);

        context = getApplicationContext();


    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return context;
    }

    private void createWorkingDir(File dir) {
        if (!dir.exists())
            dir.mkdirs();
    }


}
