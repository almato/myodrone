package de.fh_stralsund.myodrone.yaDroneUtil;

/**
 * Created by Max Edenharter on 16.04.15.
 *
 * Enum with drone commands
 */
public enum DroneMove {
    START, TAKEOFF, LANDING, UP, DOWN, FORWARD, BACKWARD, LEFT, RIGHT, ROTATION_LEFT, ROTATION_RIGHT, HOVER, STOP, EMERGENCY
}
