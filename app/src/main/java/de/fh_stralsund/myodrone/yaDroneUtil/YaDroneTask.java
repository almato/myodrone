package de.fh_stralsund.myodrone.yaDroneUtil;

import android.os.AsyncTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.android.BasicLogcatConfigurator;
import de.fh_stralsund.myodrone.utils.Const;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.command.LEDAnimation;

/**
 * Created by Max Edenharter on 16.04.15.
 *
 * Background Task to send commands to drone
 */
public class YaDroneTask extends AsyncTask<DroneMove, Void, Void> {


    static {
        BasicLogcatConfigurator.configureDefaultContext();
    }

    Logger logger = LoggerFactory.getLogger(YaDroneTask.class);

    private IARDrone drone;
    private CommandManager cmd;
    private int droneSpeed;

    public YaDroneTask(IARDrone drone, int droneSpeed) {
        this.drone = drone;
        this.cmd = drone.getCommandManager();
        this.droneSpeed = droneSpeed;

    }

    /**
     * Method for background operations
     * @param params contains commands
     * @return null
     */
    @Override
    protected Void doInBackground(DroneMove... params) {

        try {
            for (DroneMove move : params) {

                switch (move) {
                    case START:
                        cmd.setLedsAnimation(LEDAnimation.BLANK, 3, 1);
                        break;
                    case TAKEOFF:
                        drone.takeOff();
                        break;
                    case LANDING:
                        cmd.hover().doFor(1000);
                        drone.landing();
                        break;
                    case UP:
                        cmd.up(droneSpeed);
                        break;
                    case FORWARD:
                        cmd.forward(droneSpeed);
                        break;
                    case BACKWARD:
                        cmd.backward(droneSpeed);
                        break;
                    case LEFT:
                        cmd.goLeft(droneSpeed);
                        break;
                    case RIGHT:
                        cmd.goRight(droneSpeed);
                        break;
                    case DOWN:
                        cmd.down(droneSpeed);
                        break;
                    case ROTATION_LEFT:
                        cmd.spinLeft(Const.DEFAULT_SPEED);
                        break;
                    case ROTATION_RIGHT:
                        cmd.spinRight(Const.DEFAULT_SPEED);
                        break;
                    case STOP:
                        //drone.stop();
                        break;
                    case HOVER:
                        drone.hover();
                        break;
                    case EMERGENCY:
                        drone.reset();
                        break;
                }
            }
        } catch (Exception e) {
            logger.debug(String.valueOf(params.length), params.toString());
            logger.error("ERROR Background Task ", e.toString());
        }
        return null;
    }
}
