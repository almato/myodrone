package de.fh_stralsund.myodrone;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.android.BasicLogcatConfigurator;
import de.fh_stralsund.myodrone.controller.DroneController;
import de.fh_stralsund.myodrone.fragment.MainFragment;
import de.fh_stralsund.myodrone.services.MyoService;
import de.fh_stralsund.myodrone.utils.Const;


public class MainActivity extends Activity implements MainFragment.OnFragmentInteractionListener {

    static {
        BasicLogcatConfigurator.configureDefaultContext();
    }

    Logger logger = LoggerFactory.getLogger(MainActivity.class);

    private FragmentManager fragmentManager;
    private MainFragment fragment;

    /**
     * Basic Activity initialization
     *
     * @param savedInstanceState state of the activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();
        instantiateFragments(savedInstanceState);

    }

    /**
     * Method called, when activity was recreated
     *
     * @param inState saved state of the fragment
     */
    @Override
    protected void onRestoreInstanceState(Bundle inState) {
        instantiateFragments(inState);
    }

    /**
     * Method called, when activity goes sleep saves the current instance of the fragment
     *
     * @param outState current instance
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the fragment's instance
        fragmentManager.putFragment(outState, Const.FRAGMENT_MAIN, fragment);
    }

    /**
     * Method called, when activity are destroyed
     */
    @Override
    public void onDestroy() {
        this.stopService(new Intent(this, MyoService.class));
        super.onDestroy();
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResume()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startService(false, false);
    }

    /**
     * Method called, when activity are paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        startService(false, true);
    }

    /**
     * Method to create Options-Menu
     *
     * @param menu current menu
     * @return true to allow create menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Method called, when options-Item Selected
     *
     * @param item selected menu-item
     * @return return true, if menu-item selected, else false
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // Show the preferences menu
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivityForResult(settingsIntent, Const.SETTINGS_RESULT);
            return true;
        }

        if (id == R.id.action_exit) {
            DroneController.destroyDrone();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to start service with new configuration
     *
     * @param isPressed true, if start button pressed, else false
     * @param isPaused  true, when pause is activated, else false
     */
    private void startService(boolean isPressed, boolean isPaused) {
        Intent intent = new Intent(this, MyoService.class);
        intent.putExtra(Const.IS_PRESSED, isPressed);
        intent.putExtra(Const.IS_PAUSED, isPaused);
        this.startService(intent);
    }

    /**
     * Method Called, when start button are pressed
     *
     * @param isPressed true, if start button pressed, else false, else false
     */
    @Override
    public void onButtonPressed(boolean isPressed) {
        startService(isPressed, false);
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        Toast.makeText(this, getString(R.string.exit_warning), Toast.LENGTH_LONG).show();
    }


    /**
     * Method to initiate the Fragment of the Activity
     *
     * @param inState current state of the fragment
     */
    private void instantiateFragments(Bundle inState) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (inState != null) {
            fragment = (MainFragment) fragmentManager.getFragment(inState, Const.FRAGMENT_MAIN);
        } else {
            fragment = new MainFragment();
            fragmentTransaction.add(R.id.container, fragment, Const.FRAGMENT_MAIN);
            fragmentTransaction.commit();
        }
    }

}
