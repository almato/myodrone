package de.fh_stralsund.myodrone.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.thalmic.myo.AbstractDeviceListener;
import com.thalmic.myo.Arm;
import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.Pose;
import com.thalmic.myo.Quaternion;
import com.thalmic.myo.Vector3;
import com.thalmic.myo.XDirection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.android.BasicLogcatConfigurator;
import de.fh_stralsund.myodrone.controller.DroneController;
import de.fh_stralsund.myodrone.utils.Const;
import de.fh_stralsund.myodrone.utils.PrefAdapter;
import de.fh_stralsund.myodrone.yaDroneUtil.DroneMove;
import de.fh_stralsund.myodrone.yaDroneUtil.YaDroneTask;
import de.yadrone.base.IARDrone;
import de.yadrone.base.navdata.ControlState;
import de.yadrone.base.navdata.DroneState;
import de.yadrone.base.navdata.StateListener;

/**
 * Created by Alexander Zaak on 12.05.15.
 *
 * Service to convert Myo gesture to drone commands
 */
public class MyoService extends Service {

    static {
        BasicLogcatConfigurator.configureDefaultContext();
    }

    Logger logger = LoggerFactory.getLogger(MyoService.class);

    private IARDrone drone;

    private boolean isFlying = false;
    private boolean isPressed = false;
    private boolean isMyoConnected = false;
    private boolean isMyoSynced = false;
    private boolean isPaused = false;
    private int myoPose;
    private PrefAdapter prefAdapter;

    /**
     *  Method to create the service and init myo Listener
     */
    @Override
    public void onCreate() {
        super.onCreate();

        logger.debug("Service created");

        prefAdapter = new PrefAdapter(this);


        // First, we initialize the Hub singleton with an application identifier.
        Hub hub = Hub.getInstance();
        if (!hub.init(this, getPackageName())) {
            // We can't do anything with the Myo device if the Hub can't be initialized, so exit.
            Toast.makeText(this, "Couldn't initialize Hub", Toast.LENGTH_SHORT).show();
            logger.error("Couldn't initialize Hub");
            stopSelf();
            return;
        }
        // Disable standard Myo locking policy. All POSES will be delivered.
        hub.setLockingPolicy(Hub.LockingPolicy.NONE);

        // Next, register for DeviceListener callbacks.
        hub.addListener(myoListener);

        sendBroadcast();
    }


    /**
     * Called by the system every time a client explicitly starts the service by calling
     * {@link android.content.Context#startService}, providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     * <p/>
     * <p>For backwards compatibility, the default implementation calls
     * {@link #onStart} and returns either {@link #START_STICKY}
     * or {@link #START_STICKY_COMPATIBILITY}.
     * <p/>
     * <p>If you need your application to run on platform versions prior to API
     * level 5, you can use the following model to handle the older {@link #onStart}
     * callback in that case.  The <code>handleCommand</code> method is implemented by
     * you as appropriate:
     * <p/>
     * {@sample development/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.java
     * start_compatibility}
     * <p/>
     * <p class="caution">Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use {@link android.os.AsyncTask}.</p>
     *
     * @param intent  The Intent supplied to {@link android.content.Context#startService},
     *                as given.  This may be null if the service is being restarted after
     *                its process has gone away, and it had previously returned anything
     *                except {@link #START_STICKY_COMPATIBILITY}.
     * @param flags   Additional data about this start request.  Currently either
     *                0, {@link #START_FLAG_REDELIVERY}, or {@link #START_FLAG_RETRY}.
     * @param startId A unique integer representing this specific request to
     *                start.  Use with {@link #stopSelfResult(int)}.
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the {@link #START_CONTINUATION_MASK} bits.
     * @see #stopSelfResult(int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        boolean initialized = intent.getBooleanExtra(Const.IS_INITIALIZED, false);
        isPaused = intent.getBooleanExtra(Const.IS_PAUSED, false);
        if (initialized) {
            sendBroadcast();
        } else {
            isPressed = intent.getBooleanExtra(Const.IS_PRESSED, false);
            drone = DroneController.getARDrone();
            if (drone != null)
                drone.getNavDataManager().addStateListener(stateListener());
        }
        return START_REDELIVER_INTENT;
    }

    /**
     * Method is called when the service is destroyed
     */
    @Override
    public void onDestroy() {
        // We don't want any callbacks when the Service is gone, so unregister the listener.
        Hub.getInstance().removeListener(myoListener);
        Hub.getInstance().shutdown();

        if (isFlying) {
            // drone.landing();
            logger.debug("Landing!!");
        }
        logger.debug("onDestroy");
        super.onDestroy();
    }

    /**
     * MyoListener to get data from myo
     */
    private DeviceListener myoListener = new AbstractDeviceListener() {
        @Override
        public void onAttach(Myo myo, long l) {
            logger.debug("onAttach");
        }

        @Override
        public void onDetach(Myo myo, long l) {
            logger.debug("onDetach");
        }

        @Override
        public void onConnect(Myo myo, long l) {
            isMyoConnected = true;
            logger.debug("Connected");
            sendBroadcast();
        }

        @Override
        public void onDisconnect(Myo myo, long l) {
            isMyoConnected = false;
            logger.debug("Disconnected");
            sendBroadcast();
        }

        @Override
        public void onArmSync(Myo myo, long l, Arm arm, XDirection xDirection) {
            isMyoSynced = true;
            logger.debug(arm.name() + " Synced");
            sendBroadcast();
        }

        @Override
        public void onArmUnsync(Myo myo, long l) {
            isMyoSynced = false;
            logger.debug("Unsynced");
            sendBroadcast();
        }

        @Override
        public void onUnlock(Myo myo, long l) {

        }

        @Override
        public void onLock(Myo myo, long l) {

        }

        @Override
        public void onPose(Myo myo, long l, Pose pose) {
            if (pose == Pose.DOUBLE_TAP) {
                if (!isFlying && isPressed) {
                    YaDroneTask yaDroneTask = new YaDroneTask(drone, prefAdapter.getDroneSpeed());
                    yaDroneTask.execute(DroneMove.TAKEOFF, DroneMove.HOVER);
                }
            }
            myoPose = pose.ordinal();
            sendBroadcast();
        }

        @Override
        public void onOrientationData(Myo myo, long l, Quaternion quaternion) {
            // Calculate Euler angles (roll, pitch, and yaw) from the quaternion.
            float roll = (float) Math.toDegrees(Quaternion.roll(quaternion));
            float pitch = (float) Math.toDegrees(Quaternion.pitch(quaternion));

            sendBroadcast();


            if (isFlying) {
                YaDroneTask yaDroneTask = new YaDroneTask(drone, prefAdapter.getDroneSpeed());
                if (isPaused) {
                    yaDroneTask.execute(DroneMove.HOVER);
                    return;
                }

                switch (myo.getPose()) {
                    case UNKNOWN:
                    case REST:
                        yaDroneTask.execute(DroneMove.HOVER);
                        break;
                    case FIST:
                        yaDroneTask.execute(moveInDirections(roll, pitch));
                        break;
                    case FINGERS_SPREAD:
                        yaDroneTask.execute(moveUpDown(pitch));
                        break;
                    case WAVE_IN:
                        yaDroneTask.execute(DroneMove.ROTATION_LEFT);
                        break;
                    case WAVE_OUT:
                        yaDroneTask.execute(DroneMove.ROTATION_RIGHT);
                        break;
                    case DOUBLE_TAP:
                        if (pitch > 65) {
                            yaDroneTask.execute(DroneMove.LANDING);
                        }
                        break;
                }
            }
        }


        @Override
        public void onAccelerometerData(Myo myo, long l, Vector3 vector3) {

        }

        @Override
        public void onGyroscopeData(Myo myo, long l, Vector3 vector3) {

        }

        @Override
        public void onRssi(Myo myo, long l, int i) {
        }


        private DroneMove moveUpDown(float pitch) {
            if (pitch < -10) {
                //logger.debug(String.format("UP: pitch: %.0f", pitch));
                return DroneMove.UP;
            } else if (pitch > +15) {
                //logger.debug(String.format("DOWN: pitch: %.0f", pitch));
                return DroneMove.DOWN;
            } else {
                //logger.debug(String.format("HOVER: pitch: %.0f", pitch));
                return DroneMove.HOVER;
            }
        }

        private DroneMove moveInDirections(float roll, float pitch) {
            if (pitch > -40 && pitch < -10) {
                //logger.debug(String.format("Forward: pitch: %.0f, roll: %.0f ", pitch, roll));
                return DroneMove.FORWARD;
            } else if (pitch < -50) {
                // logger.debug(String.format("Backward: pitch: %.0f, roll: %.0f ", pitch, roll));
                return DroneMove.BACKWARD;
            } else if (pitch > -10 && roll > 30) {
                // logger.debug(String.format("Right: pitch: %.0f, roll: %.0f ", pitch, roll));
                return DroneMove.RIGHT;

            } else if (pitch > -10 && roll < -20) {
                // logger.debug(String.format("LEFT: pitch: %.0f, roll: %.0f ", pitch, roll));
                return DroneMove.LEFT;

            } else {
                // logger.debug(String.format("HOVER: pitch: %.0f, roll: %.0f ", pitch, roll));
                return DroneMove.HOVER;
            }
        }
    };

    /**
     * Method to send broadcasts
     */
    private void sendBroadcast() {
        Intent i = new Intent(Const.SERVICE_STATUS);
        i.putExtra(Const.IS_MYO_CONNECTED, isMyoConnected);
        i.putExtra(Const.IS_MYO_SYNCED, isMyoSynced);
        i.putExtra(Const.MYO_POSE, myoPose);

        sendBroadcast(i);
    }

    /**
     * Drone state Listener to determine whether the drone flies
     * @return
     */
    private StateListener stateListener() {
        return new StateListener() {
            @Override
            public void stateChanged(DroneState droneState) {
                if (droneState.isFlying()) {
                    logger.info("isFlying: " + droneState.isFlying());
                }
                isFlying = droneState.isFlying();
            }

            @Override
            public void controlStateChanged(ControlState controlState) {

            }
        };
    }


    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link android.os.IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p/>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link android.content.Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
